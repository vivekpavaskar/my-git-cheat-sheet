How to contribute
================================
1. Fork this repo into your github account.
2. Clone down this repository to your system.
3. Update your new commands into the [README.md](https://github.com/vivekpavaskar/git-cheat-sheet) file according to github [markdown syatax](https://guides.github.com/features/mastering-markdown/).
4. Add your information into the [CONTRIBUTERS.md](https://github.com/vivekpavaskar/git-cheat-sheet/blob/master/CONTRIBUTORS.md) file according to the following markdown syntax:
```
#### Name: Your Name
* Place: City, State, Country
* BIO: Who are you?
* GitHub Account: your github account link
```
**Note :** Add you details at the bottom of the list.

5. Create a pull request so your contributions can be merged!!!